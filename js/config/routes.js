app.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/',  {
		templateUrl: 'template/init.html',
		controller: 'initController'
	})
	.when('/learnvideo',  {
		templateUrl: 'template/learnvideo.html',
		controller: 'learnvideoController'
	})
	.when('/learn',  {
		templateUrl: 'template/learn.html',
		controller: 'learnController'
	})
	.when('/meuperfil',  {
		templateUrl: 'template/meuperfil.html',
		controller: 'perfilController'
	})
});