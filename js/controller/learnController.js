app.controller("learnController", function ($scope, $location){
	var field1 ="";
	$scope.chathidden = true;

	$scope.changeView = function(view){
    	$location.path(view);
  	}

  	$scope.hidechat = function(){
    	$scope.chathidden = !$scope.chathidden;
  	}

  	$(document).ready(function(){
	    $('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
	});
});